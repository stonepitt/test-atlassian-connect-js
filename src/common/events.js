import $ from './dollar';

var w = window;
var log = (w.AJS && w.AJS.log) || (w.console && w.console.log) || (function () {});

/**
 * A simple pub/sub event bus capable of running on either side of the XDM bridge with no external
 * JS lib dependencies.
 *
 * @param {String} key The key of the event source
 * @param {String} origin The origin of the event source
 * @constructor
 */
function Events(key, origin) {
  this._key = key;
  this._origin = origin;
  this._events = {};
  this._any = [];
}

var proto = Events.prototype;

/**
 * Subscribes a callback to an event name.
 *
 * @param {String} name The event name to subscribe the listener to
 * @param {Function} listener A listener callback to subscribe to the event name
 * @returns {Events} This Events instance
 */
proto.on = function (name, listener) {
  if (name && listener) {
    this._listeners(name).push(listener);
  }
  return this;
};

/**
 * Subscribes a callback to an event name, removing the it once fired.
 *
 * @param {String} name The event name to subscribe the listener to
 * @param {Function}listener A listener callback to subscribe to the event name
 * @returns {Events} This Events instance
 */
proto.once = function (name, listener) {
  var self = this;
  var interceptor = function () {
    self.off(name, interceptor);
    listener.apply(null, arguments);
  };
  this.on(name, interceptor);
  return this;
};

/**
 * Subscribes a callback to all events, regardless of name.
 *
 * @param {Function} listener A listener callback to subscribe for any event name
 * @returns {Events} This Events instance
 */
proto.onAny = function (listener) {
  this._any.push(listener);
  return this;
};

/**
 * Unsubscribes a callback to an event name.
 *
 * @param {String} name The event name to unsubscribe the listener from
 * @param {Function} listener The listener callback to unsubscribe from the event name
 * @returns {Events} This Events instance
 */
proto.off = function (name, listener) {
  var all = this._events[name];
  if (all) {
    var i = $.inArray(listener, all);
    if (i >= 0) {
      all.splice(i, 1);
    }
    if (all.length === 0) {
      delete this._events[name];
    }
  }
  return this;
};

/**
 * Unsubscribes all callbacks from an event name, or unsubscribes all event-name-specific listeners
 * if no name if given.
 *
 * @param {String} [name] The event name to unsubscribe all listeners from
 * @returns {Events} This Events instance
 */
proto.offAll = function (name) {
  if (name) {
    delete this._events[name];
  } else {
    this._events = {};
  }
  return this;
};

/**
 * Unsubscribes a callback from the set of 'any' event listeners.
 *
 * @param {Function} listener A listener callback to unsubscribe from any event name
 * @returns {Events} This Events instance
 */
proto.offAny = function (listener) {
  var any = this._any;
  var i = $.inArray(listener, any);
  if (i >= 0) {
    any.splice(i, 1);
  }
  return this;
};

/**
 * Emits an event on this bus, firing listeners by name as well as all 'any' listeners. Arguments following the
 * name parameter are captured and passed to listeners.  The last argument received by all listeners after the
 * unpacked arguments array will be the fired event object itself, which can be useful for reacting to event
 * metadata (e.g. the bus's namespace).
 *
 * @param {String} name The name of event to emit
 * @param {Array.<String>} args 0 or more additional data arguments to deliver with the event
 * @returns {Events} This Events instance
 */
proto.emit = function (name) {
  return this._emitEvent(this._event.apply(this, arguments));
};

/**
 * Creates an opaque event object from an argument list containing at least a name, and optionally additional
 * event payload arguments.
 *
 * @param {String} name The name of event to emit
 * @param {Array.<String>} args 0 or more additional data arguments to deliver with the event
 * @returns {Object} A new event object
 * @private
 */
proto._event = function (name) {
  return {
    name: name,
    args: [].slice.call(arguments, 1),
    attrs: {},
    source: {
      key: this._key,
      origin: this._origin
    }
  };
};

/**
 * Emits a previously-constructed event object to all listeners.
 *
 * @param {Object} event The event object to emit
 * @param {String} event.name The name of the event
 * @param {Object} event.source Metadata about the original source of the event, containing key and origin
 * @param {Array} event.args The args passed to emit, to be delivered to listeners
 * @returns {Events} This Events instance
 * @private
 */
proto._emitEvent = function (event) {
  var args = event.args.concat(event);
  fire(this._listeners(event.name), args);
  fire(this._any, [event.name].concat(args));
  return this;
};

/**
 * Returns an array of listeners by event name, creating a new name array if none are found.
 *
 * @param {String} name The event name for which listeners should be returned
 * @returns {Array} An array of listeners; empty if none are registered
 * @private
 */
proto._listeners = function (name) {
  return this._events[name] = this._events[name] || [];
};

// Internal helper for firing an event to an array of listeners
function fire(listeners, args) {
  for (var i = 0; i < listeners.length; ++i) {
    try {
      listeners[i].apply(null, args);
    } catch (e) {
      log(e.stack || e.message || e);
    }
  }
}

export default {Events: Events}